export const LAYOUT = {
  NAKED: 'Naked',
  DEFAULT: 'Default',
} as const

type LayoutKeys = keyof typeof LAYOUT
export type Layout = (typeof LAYOUT)[LayoutKeys]
