import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import ExampleComponentSimple from '@/components/ExampleComponentSimple/ExampleComponentSimple.vue'

const meta = {
  ...generateStoryMeta(ExampleComponentSimple),

  title: 'ExampleComponentSimple',
  component: ExampleComponentSimple,
} satisfies Meta<typeof ExampleComponentSimple>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  ...generateStory(),
}
