import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import ExampleComponent from './ExampleComponent.vue'

const meta = {
  ...generateStoryMeta(ExampleComponent, {
    actions: ['edit'],
  }),

  title: 'ExampleComponent',
  component: ExampleComponent,

  args: {
    greeting: 'Bla',
    checked: true,
    value: 'test',
    action: 'edit',
  },
} satisfies Meta<typeof ExampleComponent>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  ...generateStory(),

  args: {
    greeting: 'Howdy',
  },
}
