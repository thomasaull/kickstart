import { setup, fromPromise } from 'xstate'

export const ExampleState = setup({
  types: {} as {
    context: { feedback: string }
    events: { type: 'go' } | { type: 'to.default' }
  },

  // https://stately.ai/blog/2024-02-02-migrating-machines-to-xstate-v5#2-move-types-to-setup-and-remove-typegenb
  actions: {
    blupp: () => {
      throw new Error('Not implemented')
    },
  },

  actors: {
    fetchSomething: fromPromise<void, { inputExample: string }>(async () => {
      throw new Error('fetchSomething not implemented')
    }),
  },
}).createMachine({
  id: 'ExampleXstate5PureState',
  initial: 'default',

  context: {
    feedback: 'jojo passt',
  },

  states: {
    default: {
      entry: 'blupp',

      meta: {
        title: 'Its the default, how exciting!',
      },

      on: {
        go: {
          target: 'anotherState',
        },
      },
    },

    anotherState: {
      initial: 'blupp',

      meta: {
        title: 'Meta for "anotherState"',
      },

      states: {
        bla: {
          initial: 'child',
          meta: {
            message: 'hello from bla',
          },

          states: {
            child: {},
            wild: {},
          },
        },

        blupp: {
          meta: {
            message: 'hello from blupp',
          },

          on: {
            'to.default': {
              target: '#ExampleXstate5PureState.default',
            },
          },
        },
      },
    },
  },
})
