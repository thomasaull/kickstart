import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import VkIconPure from '@/components/basic/VkIcon/VkIconPure.vue'

const meta = {
  ...generateStoryMeta(VkIconPure, {}),

  title: 'VkIconPure',
  component: VkIconPure,

  args: {
    name: 'settings',
  },
} satisfies Meta<typeof VkIconPure>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  ...generateStory(),
}
