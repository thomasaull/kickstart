import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import VkSvgSpritePure from '@/components/basic/VkSvgSprite/VkSvgSpritePure.vue'

const meta = {
  ...generateStoryMeta(VkSvgSpritePure, {}),

  title: 'VkSvgSpritePure',
  component: VkSvgSpritePure,
} satisfies Meta<typeof VkSvgSpritePure>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  ...generateStory(),
}
