#!/usr/bin/env bash

# Adapted from: https://github.com/ueberdosis/laravel-docker-health-check
set -e

if [ $(curl -o /dev/null -s -w "%{http_code}\n" http://localhost) = "200" ]; then
    echo 'is healty'
    exit 0
else
    echo 'nope…'
    exit 1
fi
