import { fileURLToPath, URL } from 'node:url'
import { defineConfig, type PluginOption, loadEnv, mergeConfig } from 'vite'
import graphql from '@rollup/plugin-graphql'

import { createViteConfig } from '@thomasaull-kickstart/vite-config'

export const plugins: PluginOption[] = [graphql()]

export const configOverrides = defineConfig((options) => {
  const env = loadEnv(options.mode, process.cwd(), '')

  return {
    plugins: plugins,

    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      },
    },
  }
})

export default defineConfig((configEnv) =>
  mergeConfig(createViteConfig(configEnv, {}), configOverrides(configEnv))
)
