import { type Preview, defaultConfig, setup } from '@thomasaull-kickstart/storybook-config/preview'

import { attachStuffToAppInstance } from '@/shared'

const preview: Preview = {
  ...defaultConfig,
}

setup((app) => {
  attachStuffToAppInstance(app)
})

export default preview
