import {
  defaultConfig,
  createStoriesConfig,
  type StorybookConfig,
} from '@thomasaull-kickstart/storybook-config/main'

const config: StorybookConfig = {
  ...defaultConfig,
  stories: createStoriesConfig({ storiesPaths: ['../src'] }),
}

export default config
