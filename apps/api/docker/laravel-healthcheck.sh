#!/usr/bin/env bash

# Adapted from: https://github.com/ueberdosis/laravel-docker-health-check
set -e

if [ $(curl -o /dev/null -s -w "%{http_code}\n" http://localhost/up) = "200" ]; then
    exit 0
else
    exit 1
fi
