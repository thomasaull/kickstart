#!/bin/sh
# Runs some more laravel related startup stuff
# See: https://github.com/serversideup/docker-php/blob/main/src/common/etc/entrypoint.d/50-laravel-automations.sh

# Set default values
: "${AUTORUN_ENABLED:=false}"

# Check to see if an Artisan file exists and assume it means Laravel is configured.
if [ -f "$APP_BASE_DIR/artisan" ] && [ "$AUTORUN_ENABLED" = "true" ]; then
    # ---------------------------------------------------------------------------
    # Clear cache
    # Clears config, events, routes, views and removes cache and compiled
    # ---------------------------------------------------------------------------
    echo "🚀 Clear cache..."
    php "$APP_BASE_DIR/artisan"  optimize:clear

    # ---------------------------------------------------------------------------
    # Cache filament stuff
    # ---------------------------------------------------------------------------
    if [ "${AUTORUN_LARAVEL_CACHE_FILAMENT:=false}" = "true" ]; then
        echo "🚀 Cache filament components..."
        php "$APP_BASE_DIR/artisan" filament:cache-components
    fi

    # ---------------------------------------------------------------------------
    # Cache icons
    # ---------------------------------------------------------------------------
    if [ "${AUTORUN_LARAVEL_CACHE_ICONS:=false}" = "true" ]; then
        echo "🚀 Cache icons..."
        php "$APP_BASE_DIR/artisan" icons:cache
    fi

    # ---------------------------------------------------------------------------
    # Cache lighthouse
    # ---------------------------------------------------------------------------
    if [ "${AUTORUN_LARAVEL_CACHE_ICONS:=false}" = "true" ]; then
        echo "🚀 Cache lighthouse..."
        php "$APP_BASE_DIR/artisan" lighthouse:cache
    fi

    # ---------------------------------------------------------------------------
    # Cache everything else
    # CAches config, events, routes, views
    # ---------------------------------------------------------------------------
    echo "🚀 Cache everything else (config, events, routes, views)..."
    php "$APP_BASE_DIR/artisan" optimize
fi
