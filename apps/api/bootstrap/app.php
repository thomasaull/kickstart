<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

/**
 * Load different .env files based on APP_ENV
 *
 * The way this works is:
 * - APP_ENV is defined as an environment variable in docker-compose, because of that it's available early
 * - Larvel loads the environment based on this, e.g. APP_ENV=production → .env.production
 * - All we have to do is to load the `.env.*.local` file which provides secrets and overrides,
 *   that's what we do here:
 *
 * @see  https://nono.ma/load-different-env-to-support-multiple-domains-in-a-laravel-application
 */
if (!function_exists("loadAdditionalEnvironment")) {
    function loadAdditionalEnvironment(string $filename) {
        $basePath = __DIR__ . "/../";
        $dotenv = \Dotenv\Dotenv::createImmutable($basePath, $filename);

        try {
            $dotenv->load();
        } catch (\Dotenv\Exception\InvalidPathException $e) {
            echo "Could not find .env file";
            exit();
        }
    }
}

$environment = env("APP_ENV");

if ($environment === "development") {
    loadAdditionalEnvironment(".env.development.local");
}

if ($environment === "production") {
    loadAdditionalEnvironment(".env.production.local");
}

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__ . "/../routes/web.php",
        api: __DIR__ . "/../routes/api.php",
        commands: __DIR__ . "/../routes/console.php",
        health: "/up",
    )
    ->withMiddleware(function (Middleware $middleware) {
        // $middleware->api(prepend: [
        //     \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
        // ]);
        // $middleware->statefulApi();
        // $middleware->trustProxies(at: "*");
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })
    ->create();
