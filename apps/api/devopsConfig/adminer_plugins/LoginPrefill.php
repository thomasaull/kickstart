<?php

/**
 * @see https://github.com/TimWolla/docker-adminer/issues/42#issuecomment-836043863
 */

class LoginPrefill {
  /** Get login form field
   * @param string
   * @param string HTML
   * @param string HTML
   * @return string
   */
  function loginFormField($name, $heading, $value) {
    switch ($name) {
      case "server":
        return $heading . $this->_fill($value, "ADMINER_SERVER");
        break;

      case "username":
        return $heading . $this->_fill($value, "ADMINER_USER");
        break;

      case "password":
        return $heading . $this->_fill($value, "ADMINER_PASSWORD");
        break;

      case "db":
        return $heading . $this->_fill($value, "ADMINER_DB");
        break;

      default:
        return $heading . $value;
        break;
    }
  }

  /** Name in title and navigation
   * @return string HTML code
   */
  function name() {
    $warning =
      '<span style="color:red; font-size: 0.6em; margin: 0 3px">* Home env.</span>';

    return '<a href="https://www.' .
      $_ENV["DOMAIN"] .
      '"' .
      target_blank() .
      ' id="h1">' .
      $_ENV["APP_NAME"] .
      "</a>" .
      $warning;
  }

  /**
   * Fill value from environment.
   */
  private function _fill($value, $key) {
    if (strpos($value, "value=") === false) {
      $tail = strpos($value, ">");
      $prefilled = substr_replace(
        $value,
        ' value="' . htmlspecialchars($_ENV[$key]) . '"',
        $tail,
        0,
      );

      // Password might hold quotes or other symbols that break formatting.
      return $prefilled;
    }

    return str_replace('value=""', 'value="' . $_ENV[$key] . '"', $value);
  }
}

return new LoginPrefill();
