## Setup 

### Install composer dependencies without composer installed
```bash
docker run --rm \
  --user "$(id -u):$(id -g)" \
  --volume "$(pwd):/var/www/html" \
  --workdir /var/www/html \
  laravelsail/php82-composer:latest \
  composer install --ignore-platform-reqs
```

### Install laravel with help of docker 
```bash
docker run --rm \
  --user "$(id -u):$(id -g)" \
  --volume "$(pwd):/var/www/html" \
  --workdir /var/www/html \
  laravelsail/php82-composer:latest \
  bash -c "laravel new example-app"
```

### ngrok
- Rename `ngrok.yml.example` to `ngrok.yml` and adjust settings
- Add your ngrok token to .env with the key `NGROK_AUTHTOKEN`
