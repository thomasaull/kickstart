/**
 * NOTE: This file needs to be compiled manually before changes will apply
 * @run pnpm run build
 *
 */
import vue from '@vitejs/plugin-vue'
import {
  // loadEnv,
  type ConfigEnv,
  type UserConfig,
  type PluginOption,
} from 'vite'
import dts from 'vite-plugin-dts'
import svgLoader from 'vite-svg-loader'
// import { visualizer } from 'rollup-plugin-visualizer'

type Options = {
  type?: 'web' | 'webComponents'
  entry?: string
}

export function createViteConfig(options: ConfigEnv, configOptions: Options) {
  const { type = 'web' } = configOptions

  const optionalPlugins: PluginOption[] = []

  // if (process.env.ANALYZE_BUNDLE === 'true') {
  //   optionalPlugins.push(
  //     visualizer({
  //       template: 'treemap',
  //       open: true,
  //       gzipSize: true,
  //       brotliSize: true,
  //     })
  //   )
  // }

  const config: UserConfig = {
    plugins: [
      vue(),

      dts({
        tsconfigPath: './tsconfig.app.json',
        insertTypesEntry: true,
      }),

      svgLoader({
        defaultImport: 'component',
        svgoConfig: {
          plugins: [
            {
              name: 'removeAttrs',
              params: {
                attrs: '(stroke|fill|stroke-width|opacity)',
              },
            },
          ],
        },
      }),

      ...optionalPlugins,
    ],

    /**
     * Inside the CI environment we set the pnpm store directory to the project root
     * This causes a "too-many-watchers" error in the pipeline.
     * Ignoring this folder solves the error.
     */
    server: {
      watch: {
        ignored: ['**/.pnpm-store/**'],
      },
    },
  }

  return config
}
