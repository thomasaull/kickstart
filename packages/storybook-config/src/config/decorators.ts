import { useParameter } from '@storybook/preview-api'
import type { StoryFn } from '@storybook/vue3'

// import StateWrapper from '../components/StateWrapper.vue'

export const withStates = (story: StoryFn) => {
  const additionalStates = useParameter('additionalStates', [])

  return {
    name: 'DecoratorWithStates',
    components: {
      story,
      // StateWrapper,
    },

    setup() {
      return {
        additionalStates,
      }
    },

    template: `
        <StateWrapper :additionalStates="additionalStates">
          <template v-slot:default="{ state }">
            <story :class="'is-'+state" />
          </template>
        </StateWrapper>
      `,
  }
}
