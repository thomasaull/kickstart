import type { StorybookConfig } from '@storybook/vue3-vite'
export type { StorybookConfig } from '@storybook/vue3-vite'
import path from 'path'
import remarkGfm from 'remark-gfm'

/**
 * This function is used to resolve the absolute path of a package.
 * It is needed in projects that use Yarn PnP or are set up within a monorepo.
 */
function getAbsolutePath(value: string): any {
  return path.dirname(require.resolve(path.join(value, 'package.json')))
}

export function createStoriesConfig(options: { storiesPaths: string[] }) {
  const stories: string[] = []

  // Add file endings to stories paths
  options.storiesPaths.forEach((storiesPath) => {
    stories.push(`${storiesPath}/**/*.stories.@(js|jsx|mjs|ts|tsx)`)
    stories.push(`${storiesPath}/**/*.mdx`)
  })

  return stories
}

export function createConfig(options: { storiesPaths: string[] }) {
  const config: StorybookConfig = {
    ...defaultConfig,
    stories: createStoriesConfig({ storiesPaths: options.storiesPaths }),
  }

  return config
}

type StorybookBaseConfig = Omit<StorybookConfig, 'stories'>

export const defaultConfig: StorybookBaseConfig = {
  addons: [
    getAbsolutePath('@storybook/addon-links'),
    {
      name: '@storybook/addon-essentials',
      options: {
        docs: false,
      },
    },
    {
      name: '@storybook/addon-docs',
      options: {
        mdxPluginOptions: {
          mdxCompileOptions: {
            remarkPlugins: [remarkGfm],
          },
        },
      },
    },
    getAbsolutePath('@storybook/addon-interactions'),
  ],

  framework: {
    name: getAbsolutePath('@storybook/vue3-vite'),
    // name: '@storybook/vue3-vite',
    options: {
      docgen: 'vue-component-meta',
    },
  },

  docs: {
    autodocs: false,
  },
}
