import { type Preview } from '@storybook/vue3'
export { setup, type Preview, type Decorator } from '@storybook/vue3'
import { useParameter } from '@storybook/preview-api'

export const defaultConfig: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },

  globalTypes: {},

  decorators: [
    (story, context) => {
      const layout = useParameter('appLayout', null)

      return {
        name: 'DecoratorMain',
        components: {
          // StorybookMain,
          story,
        },

        setup() {
          return {
            layout,
          }
        },

        template: `
          <StorybookMain
            :isStorybook="true"
            :storybookAppLayout="layout"
          >
            <story/>
          </StorybookMain>
        `,
      }
    },
  ],
}
