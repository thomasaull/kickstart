import '@storybook/types'
import type { StorybookParameters } from '@storybook/types'

declare module '@storybook/types' {
  interface Parameters extends StorybookParameters {
    // Add your parameter types here
  }
}
