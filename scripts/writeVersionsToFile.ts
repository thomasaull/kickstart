/**
 * Write version of package.json to VERSION file
 */
import path from 'node:path'
import fs from 'node:fs'
import { readPackageSync } from 'read-pkg'
import { glob } from 'glob'

async function run() {
  const apps = await glob(`./apps/*/`)
  const packages = await glob(`./packages/*/`)

  const allDirectories = [...apps, ...packages]

  allDirectories.forEach((directory) => {
    writeVersion(directory)
  })
}

function writeVersion(directory: string) {
  const pathNormalized = path.join(directory, 'VERSION')
  const pkg = readPackageSync({ cwd: directory })
  const version = pkg.version

  console.log('write version', version, 'to', pathNormalized)

  fs.writeFileSync(pathNormalized, version)
}

run()
