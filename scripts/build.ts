import { $ } from 'execa'
import consola from 'consola'

async function buildPackage(directory: string) {
  consola.start(`Building: ${directory}`)
  await $({ cwd: directory })`pnpm run build`.pipeStdout?.(process.stdout)
  consola.success(`${directory} built`)
}

/**
 * Build the packages which should be released via the package registry
 */
async function run() {
  consola.start('Run setup')
  await $`pnpm run setup`
  consola.success('Done')

  /**
   * Run linting without auto-fix so that we don't get any magically
   * changing code when building or releasing
   */
  consola.start('Run linters')
  await $`pnpm run lint`
  consola.start('Done')

  // buildPackage('path/to/package')
}

run()
