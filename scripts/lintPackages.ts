/**
 * @todo Maybe use https://www.npmjs.com/package/@manypkg/get-packages
 * */

// import { eq } from 'semver'
// import { load } from 'js-yaml'
// import * as fs from 'fs'

// interface Rule {
//   packageName: string
//   version: string
//   message: string
// }

// const rules: Rule[] = [
//   {
//     packageName: '@astrojs/image',
//     version: '0.3.4',
//     message:
//       'Check if `npm run astro:build works with the updates images package',
//   },
//   // {
//   //   packageName: 'astro',
//   //   version: '1.0.8',
//   //   message: '1.0.9 breaks the usage of global scss imports in vite.config.js',
//   // },
// ]

// interface Error {
//   rule: Rule
//   usedVersion: string
// }

// const errors: Error[] = []

// function checkVersion({ rule, packages }: { rule: Rule; packages: string[] }) {
//   const packageString = packages.find((item) => {
//     return item.startsWith(`/${rule.packageName}`)
//   })

//   if (!packageString) return

//   // https://regex101.com/r/8S32Gd/3
//   const regex = /^\/(?<name>@?[^@]*)@(?<version>\d+\.\d+\.\d+\.?)/m
//   const usedVersion = regex.exec(packageString)?.groups?.version

//   if (!usedVersion) {
//     throw new Error('Extraction of version was not successful')
//   }

//   const isSameVersion = eq(usedVersion, rule.version)
//   if (isSameVersion) return

//   errors.push({
//     rule: rule,
//     usedVersion: usedVersion,
//   })
// }

// function checkVersions() {
//   const file = load(fs.readFileSync('../../pnpm-lock.yaml', 'utf8'))

//   rules.forEach((rule) =>
//     checkVersion({ rule, packages: Object.keys(file.packages) })
//   )

//   if (errors.length === 0) return

//   // Display error
//   console.log('Lint packages errors:')
//   console.log('-------------------------------------------')
//   errors.forEach((error) => {
//     console.log(
//       `- ${error.rule.packageName} ${error.rule.version} (Version ${error.usedVersion} used): ${error.rule.message}`
//     )
//   })
//   console.log('-------------------------------------------')

//   throw new Error('There are some problems with package versions')
// }

// checkVersions()
