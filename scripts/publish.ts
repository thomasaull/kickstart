import { $ } from 'execa'
import consola from 'consola'

async function run() {
  consola.start('Run build')
  await $`pnpm run build`
  consola.success('Done')

  consola.start('Publish packages')
  // await $`pnpm publish --recursive --no-git-checks`.pipeStdout?.(process.stdout)
  await $`pnpm changeset publish`.pipeStdout?.(process.stdout)
  consola.start('Done')
}

run()
