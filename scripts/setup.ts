/**
 * Run initial scripts necessary to start development
 */

import { $ } from 'execa'
import consola from 'consola'

async function run() {
  consola.start('Build vite-config')
  await $({ cwd: './packages/vite-config' })`pnpm run build`
  consola.success('Done')

  consola.start('Run generators')
  await $`pnpm run generate`
  consola.success('Done')
}

run()
