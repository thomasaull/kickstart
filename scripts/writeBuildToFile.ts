/**
 * Write build to BUILD file
 */
import path from 'node:path'
import fs from 'node:fs'
import minimist from 'minimist'

const argv = minimist<{
  build: string | number
  directory: string
}>(process.argv.slice(2), { string: ['_'] })

const build = argv.build
if (!build) throw new Error('build parameter is missing')

const directory = argv.directory
if (!build) throw new Error('directory parameter is missing')

// Check if directory exists
if (!fs.existsSync(directory)) {
  throw new Error(`directory ${directory} does not exist`)
}

const pathNormalized = path.join(directory, 'BUILD')
console.log(`write build "${build}" to "${pathNormalized}"`)

fs.writeFileSync(pathNormalized, build.toString())
