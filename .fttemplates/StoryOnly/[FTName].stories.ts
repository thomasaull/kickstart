import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import [FTName] from '@/components/[FTName]/[FTName].vue'

const meta = {
  ...generateStoryMeta([FTName]),

  title: '[FTName]',
  component: [FTName],
} satisfies Meta<typeof [FTName]>

export default meta
type Story = StoryObj<typeof meta>


export const Default: Story = {
  ...generateStory()
}
