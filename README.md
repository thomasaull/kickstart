# Setup

## Gitlab CI

Note: `changesets-gitlab` theoretically supports using a `CI_JOB_TOKEN` but since the `comment` task needs `api` access this is blocked by this issue: https://gitlab.com/groups/gitlab-org/-/epics/3559

1. Create a personal access token
1. Add CI Variable `GITLAB_TOKEN` with the value of the personal access token

## Packages

1. Add `.npmrc` file with the following contents:

```
@thomasaull-shared:registry=https://gitlab.com/api/v4/projects/55498764/packages/npm/
```

## Rename everything

Rename `@thomasaull-kickstart` to whatever your project name is

## Delete Git History

If you've cloned the project, you might want to delete the git history by removing the `.git` folder and initializing a fresh repository:

```bash
rm -rf .git
git init
git add .
git commit -m "🎉 Initial commit"
```

## IDE

- Use Takeover mode: https://vuejs.org/guide/typescript/overview.html#takeover-mode

# Misc

## Filter commits from `git blame`

- Update `.git-blame-ignore-revs`
- Add the following to `/git/config`

```
[blame]
	ignoreRevsFile = .git-blame-ignore-revs
```

## htaccess for single page applications

See: https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations

## Deployment Checklist

- [ ] Update server ip in `gitlab-ci-templates/variables.yml`
- [ ] Make sure `SSH_LOGIN` is correct in `gitlab-ci-templates/variables.yml`
- [ ] Update `FOLDER_SERVICE` in all `gitlab-ci.yml` files
- [ ] Add `SSH_DEPLOYER_PRIVATE_KEY` CI Variable in gitlab
