/* eslint-env node */

module.exports = {
  '*.{.vue,js,jsx,cjs,mjs,ts,tsx,cts,mts}': 'eslint --fix',
  '*.{vue,css,scss}': 'stylelint --fix --allow-empty-input',
  '*': 'prettier --write --ignore-unknown',
}
